using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Würfelvolumen_CLI
{
    class Program
    {
        static void Main(string[] args)
        {

            // lokale Variablen
            double seitenlaenge, volumen;              // Variablendeklaration: Abschluss mit Semikolon
            string eingabe, ausgabe;                   // Variablen vom Typ Text

            // Eingabe
            // Benutzerführung
            Console.Write("Seitenlänge: ");
            // Benutzereingabe
            eingabe = Console.ReadLine();             // Das = ist ein Zuweisungsoperator
            // Eingabe im Typ Double konvertieren
            seitenlaenge = Convert.ToDouble(eingabe);

            // Verarbeitung
            volumen = Math.Pow(seitenlaenge, 3); // Funktion zum potenzieren eines Wertes

            // Ausgabe
            Console.Write("Volumen: ");
            // Ergebnis anzeigen mit zwei Dezimalstellen
            Console.WriteLine(volumen.ToString("F2"));

            // Programmausgabe stoppen
            Console.Write("... weiter mit beliebiger Taste.");
            Console.ReadKey();
        }
    }
}
